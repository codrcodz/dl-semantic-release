# [1.1.0](https://gitlab.com/codrcodz/dl-semantic-release/compare/v1.0.0...v1.1.0) (2019-08-21)


### Features

* Add wrapper for semantic-release ([ad1361f](https://gitlab.com/codrcodz/dl-semantic-release/commit/ad1361f))

# 1.0.0 (2019-08-02)


### Features

* initial release ([1409705](https://gitlab.com/dreamer-labs/dl-semantic-release/commit/1409705))
